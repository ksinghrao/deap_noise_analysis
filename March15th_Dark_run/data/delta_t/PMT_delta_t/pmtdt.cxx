/*            _   __   _   _   _    _        
        |  | |   |  | | \ |   | \  /          
        |  | |   |  | | | |   |_/  \_
        |__| |_  |__| | | |_  |\     \
        |  | |   |  | | | |   | \    |
        |  | |__ |  | |_/ |__ |  \ __/
*/

//ROOT
#include<TChain.h>
#include<TH1F.h>
#include<RAT/DS/Root.hh>
#include<RAT/DS/CAL.hh>
#include<RAT/DS/PMT.hh>
#include<RAT/DS/Pulse.hh>
#include<RAT/DS/Block.hh>
#include<TCanvas.h>
#include<TApplication.h>
#include<TH2F.h>
#include<TFile.h>
#include<TMath.h>>
#include<RAT/DS/TS.hh>
#include<TLine.h>
#include<TBox.h>
#include<TAttLine.h>
#include<TAttFill.h>
#include<TColor.h>
#include<TText.h>
#include<TAttText.h>
#include<TLatex.h>
#include<TROOT.h>
#include<RAT/DS/CAL.hh>
#include<RAT/DS/QT.hh>
#include<TDirectory.h>
#include<TNtuple.h>
#include<TTree.h>
//#include<RAT/CAENDigitizer.hh>
#include<TF1.h>


//C++
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<string>
#include<vector>
#include<sstream>
#include<math.h>


using namespace std;

#include "pmtdt.h"

/*             __
        |\ /| |  | | |\  |
        | | | |  | | | | |
        |   | |__| | | | |
        |   | |  | | | | |
        |   | |  | | |  \|
*/
int main(int argc, char ** argv){
TApplication theApp("App",&argc,argv);

sequencer();


theApp.Run();

cout << "!!!!!!!!!!!!DONE!!!!!!!!!!!!"<< endl;

return 0;

}

/*       __       __   _   __       _
	|    |   |  | | \ |  | |   /
	|    |   |  | |_/ |  | |   \_
	| _  |   |  | | \ |__| |     \
	|  | |   |  | | | |  | |     |
	|__| |__ |__| |_/ |  | |__ __/
*/


//GLOBALS

	int n_hist =255;//number of histograms
	//Int_t num_events = 50000;
	Double_t window_size = 200E3; //us
	Int_t binning = 16;
	Int_t h_bins = window_size/binning;//window size/binning
//input file


	TString inpath("/home/singhrao/Documents/DEAP_noise_analysis/March15th_Dark_run/data/delta_t/PMT_QT/PMT_QT_histograms/");   //path to CAL populated file
//output file
	TString opath("/home/singhrao/Documents/DEAP_noise_analysis/March15th_Dark_run/data/delta_t/PMT_delta_t/PMT_DT_plots/");


/*       __   __  ___  _
        |    |     |  /
        |__  |     |  \_
        |    |     |    \
        |    |     |    |
        |    |__   |  __/
*/

//
//
// sequencer
//
//

void sequencer(){

deltat();

}//sequencer



//
//
// delta t
//
//

void deltat(){
//Define 255 histograms for each PMT
	TH2D* pmtqdt[n_hist];
	TH2D* pmtq1dt[n_hist];
	TH2D *pmtqt[n_hist];
	TH2D *pmtq1t[n_hist];	
	TH1D *delta_t[n_hist];
//Variable binning
	int nBinMax=1001;
	double bin[1000];
	int nBin=0;
	double upperBound=100*pow(2.,nBin/10.);
	while(upperBound<2e5 && nBin<nBinMax){
		bin[nBin]= upperBound; 
		cout << bin[nBin] << endl;
		nBin++;
		upperBound = 100*pow(2.,nBin/10.);
	}
	bin[nBin]=upperBound;
	cout << nBin << endl;	
	
	//define the delta t histograms
	for(int a0 =0; a0<n_hist;a0++){
		//define the mean histogram for loop 0, else define PMT histograms		
//		TString qt_tag("qtall"+convertInt(a0));
//		pmtqt[a0] = new TH2D(qt_tag,qt_tag,h_bins,0,window_size,100,0,100);

		TString q1t_tag("q1t"+convertInt(a0));
		pmtq1t[a0] = new TH2D(q1t_tag,q1t_tag,nBin,bin,100,0,100);
		//pmtq1t[a0] = new TH2D(q1t_tag,q1t_tag,h_bins,0,window_size,100,0,100);
		TString q1dt_tag("q1dt"+convertInt(a0));
		pmtq1dt[a0] = new TH2D(q1dt_tag,q1dt_tag,nBin,bin,100,0,100);
		//pmtq1dt[a0] = new TH2D(q1dt_tag,q1dt_tag,h_bins,0,window_size,100,0,100);
		TString qdt_tag("qdt"+convertInt(a0));
		pmtqdt[a0] = new TH2D(qdt_tag,qdt_tag,nBin,bin,100,0,100);
		//pmtqdt[a0] = new TH2D(qdt_tag,qdt_tag,h_bins,0,window_size,100,0,100);

		TString dt_tag("delta_t"+convertInt(a0));
		delta_t[a0] = new TH1D(dt_tag,dt_tag,nBin,bin);
		//delta_t[a0] = new TH1D(dt_tag,dt_tag,h_bins,0,window_size);
		}//for

//Calculate  PMT spe charge window


	int qpmtpeak[n_hist];
	int tpmtpeak[n_hist];
	
	TH1D *qpmt[n_hist];
	TH1D *tpmt[n_hist];

	for(int iter1 =0; iter1 < n_hist; iter1++){
		
		TString qidin("PMTq" + convertInt(iter1) );
		TString qinfilenm("qPMT" + convertInt(iter1) + ".root");
		TFile *qinfile = new TFile(inpath + qinfilenm);
		
		qpmt[iter1] = (TH1D*)qinfile->Get(qidin);
		qpmtpeak[iter1] = qpmt[iter1]->GetMaximumBin();		

//		cout << qidin << "    " << qpmt[iter1]->GetMaximumBin() << endl;
		//spe charge condition
		TString tidin("PMTt" + convertInt(iter1) );
		TString tinfilenm("tPMT" + convertInt(iter1) + ".root");
		TFile *tinfile = new TFile(inpath + tinfilenm);
		
		tpmt[iter1] = (TH1D*)tinfile->Get(tidin);
		tpmtpeak[iter1] = binning*tpmt[iter1]->GetMaximumBin();
	
	}//calib

double llimit[n_hist], hlimit[n_hist];

for(int ya=0; ya< n_hist;ya++){
	llimit[ya] = qpmtpeak[ya] -9;

	hlimit[ya] = qpmtpeak[ya]+ 9 ;
	cout << "lower limit "<< llimit[ya] << " higher limit " << hlimit[ya] << " peak position "<<  qpmtpeak[ya] << " PMT number " << ya << endl;
	}

//Define histograms to be populated
//Time vs charge of first pulse for all first pulses

//Calculating Delta t
TFile *infile = new TFile(inpath +"qt.root");

TTree *qt = (TTree*)infile->Get("Dark_Run");

Double_t block_charge, block_time;
Int_t pmtidn;

qt->SetBranchAddress("pmtidn",&pmtidn);
qt->SetBranchAddress("block_charge",&block_charge);
qt->SetBranchAddress("block_time",&block_time);

int nhits = qt->GetEntries();
        int npmt=-1;
        double tFirst;
        double qFirst;
        for(int iEntry=0; iEntry<nhits; iEntry++){
                qt->GetEntry(iEntry);
                if(npmt!=pmtidn){// first pulse of the pmt
                        npmt=pmtidn;
//                      pmtqt[pmtidn]->Fill(block_time,block_charge,1);
                        if(block_charge>llimit[pmtidn] && block_charge<hlimit[pmtidn] && block_time<window_size/2){ 
                                tFirst = block_time;
                                qFirst = block_charge;
                                pmtq1t[pmtidn]->Fill(tFirst,qFirst);//fir normalization
                        }
                        else{
                                tFirst=-1;// skip event
			}
                }
                else{
                        if(tFirst>=0. && (block_time-tFirst)<window_size/2){
                                pmtq1dt[pmtidn]->Fill(block_time-tFirst,qFirst,1);
                                pmtqdt[pmtidn]->Fill(block_time-tFirst,block_charge,1);

			
				tFirst = -1;
	
                        }
                }
        }




//Compplete behaviour
//TH2D *firstqtall = new TH2D("q1t_all","q1t_all",h_bins,0,window_size,100,0,100);
//TH2D *firstqt= new TH2D("q1t","q1t",h_bins,0,window_size,40,0,40);
//TH2D *q0dt = new TH2D("q2t","q2t",h_bins,0,window_size,40,0,40);
//TH2D *qdt= new TH2D("DARKqdt","DARKqdt",h_bins,0,window_size,40,0,40);

TH1D *del_t = new TH1D("dt","dt",h_bins,0,window_size);
/*
for(int a4 =0; a4 < n_hist; a4++){
	firstqtall->Add(pmtqt[a4],1);
	firstqt->Add(pmtq1t[a4],1);
	q0dt->Add(pmtq1dt[a4],1);
	qdt->Add(pmtqdt[a4],1);

//	delta_t[a4]->Sumw2();
//	delta_t[a4]->Scale(delta_t[a4]->GetBinWidth(1)/pmtq1t[a4]->GetEntries());

	//del_t->Add(delta_t[a4],1);
}

//Create 1D histos by prjecting on X
del_t = qdt->ProjectionX();
*/
for(int a8 =0;a8<n_hist;a8++){
delta_t[a8] = pmtqdt[a8]->ProjectionX();
}

//Check
/*
TCanvas *can2 = new TCanvas("can2","can2",1200,600);
can2->Divide(4,1);
TPad *can21 = (TPad*)(can2->cd(1));
firstqtall->Draw("colz");
firstqtall->GetXaxis()->SetTitle("time (ns)");
firstqtall->GetYaxis()->SetTitle("Charge (pC)");
firstqtall->SetTitle("All first pulses");

TPad *can22 = (TPad*)(can2->cd(2));
firstqt->Draw("colz");
firstqt->GetYaxis()->SetRangeUser(0,50);
firstqt->GetXaxis()->SetTitle("time (ns)");
firstqt->GetYaxis()->SetTitle("Charge (pC)");
firstqt->SetTitle("All first pulses with SPE cut");

TPad *can23 = (TPad*)(can2->cd(3));
q0dt->Draw("colz");
q0dt->GetYaxis()->SetRangeUser(0,50);
q0dt->GetXaxis()->SetTitle("time (ns)");
q0dt->GetYaxis()->SetTitle("Charge (pC)");
q0dt->SetTitle("Second pulses");

TPad *can24 = (TPad*)(can2->cd(4));
qdt->Draw("colz");
qdt->GetYaxis()->SetRangeUser(0,400);
qdt->GetXaxis()->SetRangeUser(0,1E5);
qdt->GetXaxis()->SetTitle("delta t (ns)");
qdt->GetYaxis()->SetTitle("Charge (pC)");
qdt->SetTitle("Second pulse charge vs Delta T");
*/
//Normalize delta t for each PMT


for(int a2 =0; a2<n_hist; a2++){


	Double_t avg_nevents = pmtq1t[a2]->GetEntries();	

	delta_t[a2]->Sumw2();
	
		for(int ibin =0;ibin < delta_t[a2]->GetNbinsX();ibin++){

			delta_t[a2]->SetBinContent(ibin,delta_t[a2]->GetBinContent(ibin)/delta_t[a2]->GetBinWidth(ibin)/avg_nevents);

			delta_t[a2]->SetBinError(ibin,delta_t[a2]->GetBinError(ibin)/delta_t[a2]->GetBinWidth(ibin)/avg_nevents);
		}

	//cout << "spe like events "<< avg_nevents << " Histogram size " << delta_t[a2]->GetEntries() <<  " width weighted Integral "<< delta_t[a2]->Integral("width") << " PMT number " << a2 << endl;
	cout << " PMT number " << a2 << " width weighted integral "<< delta_t[a2]->Integral("width") << endl;
	}


//Normalize the complete delta t histogram

//cout << "norm events "<< firstqt->GetEntries() << " hist size " << del_t->GetEntries()<<  endl;
/*
del_t->Sumw2();

for(int ibin =0;ibin < del_t->GetNbinsX();ibin++){

del_t->SetBinContent(ibin,del_t->GetBinContent(ibin)/del_t->GetBinWidth(ibin)/firstqt->GetEntries());

del_t->SetBinError(ibin,del_t->GetBinError(ibin)/del_t->GetBinWidth(ibin)/firstqt->GetEntries());

}
*/
cout << "MEAN integral " << del_t->Integral("width") << endl;


//Plot and save
/*
TCanvas * ui = new TCanvas("ci","ci");
ui->Divide(1,1);
TPad *ui1 = (TPad*)(ui->cd(1));
//ui1->SetLogx();
del_t->GetXaxis()->SetRangeUser(0,100E3);
del_t->Draw();

TFile *del_to = new TFile(opath +"delta_pmt_t.root","recreate");
del_t->Write();
del_to->Close();

TFile* qtout[4];
qtout[0] = new TFile(opath + "firstpulseALL.root","recreate");
firstqtall->Write();
qtout[0]->Close();

qtout[1] = new TFile(opath + "firstpulseqt.root","recreate");
firstqt->Write();
qtout[1]->Close();

qtout[2] = new TFile(opath + "secondpulseqt.root","recreate");
q0dt->Write();
qtout[2]->Close();

qtout[3] = new TFile(opath + "secondpulseqdt.root","recreate");
qdt->Write();
qtout[3]->Close();
*/

TFile *ofile[n_hist];
TFile *ofile2[n_hist];
TFile *ofile3[n_hist];

for(int a6 =0; a6<n_hist ;a6++){
		//Write delta t
		string pmt_id = "pmt" + convertInt(a6) + ".root";
		TString rpmt_id(opath + pmt_id);
		ofile[a6] = new TFile(rpmt_id,"recreate");
		delta_t[a6]->Write();
		ofile[a6]->Close();
		
		//Write q1dt
		string pmt_id2 = "q2dtPMT" + convertInt(a6) + ".root";
		TString rpmt_id2(opath + pmt_id2);
		ofile2[a6] = new TFile(rpmt_id2,"recreate");
		pmtqdt[a6]->Write();
		ofile2[a6]->Close();
		
		//Write q0dt
		string pmt_id3 = "q1dtPMT" + convertInt(a6) + ".root";
		TString rpmt_id3(opath + pmt_id3);
		ofile3[a6] = new TFile(rpmt_id3,"recreate");
		pmtq1dt[a6]->Write();
		ofile3[a6]->Close();
		

}

cout << "!!!DONE!!!" << endl;


}//delta_t fct



//
//	
//Convert Int
//
//

string convertInt(int number){
	stringstream ss;
	ss<< number;
	string sss = ss.str();
	return sss;

}//string conversion

//
//
//sortFunc
//
//

bool sortFunc( const vector<Double_t>& p1, const vector<Double_t>& p2 ) {
 return p1[0] < p2[0];
}


//
//
// progess track
//
//

string progress(int iter, int n_events){
	int c_p,p_p;;
	string prog;
	c_p = 100*iter/n_events;

	prog = convertInt(c_p);
	prog = "We are " +  convertInt(c_p) + " % of the way there";
	return prog;
//	}
}
