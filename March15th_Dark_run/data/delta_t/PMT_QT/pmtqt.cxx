/*            _   __   _   _   _    _        
        |  | |   |  | | \ |   | \  /          
        |  | |   |  | | | |   |_/  \_
        |__| |_  |__| | | |_  |\     \
        |  | |   |  | | | |   | \    |
        |  | |__ |  | |_/ |__ |  \ __/
*/

//ROOT
#include<TChain.h>
#include<TH1F.h>
#include<RAT/DS/Root.hh>
#include<RAT/DS/CAL.hh>
#include<RAT/DS/PMT.hh>
#include<RAT/DS/Pulse.hh>
#include<RAT/DS/Block.hh>
#include<TCanvas.h>
#include<TApplication.h>
#include<TH2F.h>
#include<TFile.h>
#include<TMath.h>>
#include<RAT/DS/TS.hh>
#include<TLine.h>
#include<TBox.h>
#include<TAttLine.h>
#include<TAttFill.h>
#include<TColor.h>
#include<TText.h>
#include<TAttText.h>
#include<TLatex.h>
#include<TROOT.h>
#include<RAT/DS/CAL.hh>
#include<RAT/DS/QT.hh>
#include<TDirectory.h>
#include<TNtuple.h>
#include<TTree.h>
//#include<RAT/CAENDigitizer.hh>
#include<TF1.h>


//C++
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<string>
#include<vector>
#include<sstream>
#include<math.h>


using namespace std;

#include "pmtqt.h"

/*             __
        |\ /| |  | | |\  |
        | | | |  | | | | |
        |   | |__| | | | |
        |   | |  | | | | |
        |   | |  | | |  \|
*/
int main(int argc, char ** argv){
TApplication theApp("App",&argc,argv);

sequencer();

cout << "!!!!!!!!!!!!DONE!!!!!!!!!!!!"<< endl;

theApp.Run();


return 0;

}

/*       __       __   _   __       _
	|    |   |  | | \ |  | |   /
	|    |   |  | |_/ |  | |   \_
	| _  |   |  | | \ |__| |     \
	|  | |   |  | | | |  | |     |
	|__| |__ |__| |_/ |  | |__ __/
*/


//GLOBALS

	int n_hist =255;//number of histograms
	//Int_t num_events = 50000;
	Double_t window_size = 200E3; //us
	Double_t h_bins = 12500;//window size/binning
	Int_t binning = 16;
//Add run number here
	string run_num = "8048";

//input file


	TString path1("/home/singhrao/Documents/DEAP_noise_analysis/src_conv_midas/midas_runs/DarkRunMarch5_2014/8136/deap_CAL_008136*");   //path to CAL populated file
	TString path2("/home/singhrao/Documents/macros/DEAP_noise_analysis/src_conv_midas/midas_runs/DarkRunMarch5_2014/8137/deap_CAL_008137*");   //path to CAL populated file
//output file
	TString opath("/home/singhrao/Documents/macros/DEAP_noise_analysis/March15th_Dark_run/data/delta_t/PMT_QT/PMT_QT_histograms/");


/*       __   __  ___  _
        |    |     |  /
        |__  |     |  \_
        |    |     |    \
        |    |     |    |
        |    |__   |  __/
*/

//
//
// sequencer
//
//

void sequencer(){

qt_block();

}//sequencer



//
//
// qt block
//
//

void qt_block(){

        TChain *t = new TChain("T");

        t->Add(path1);
        t->Add(path2);

        t->GetEntries();

        RAT::DS::Root *ds = new RAT::DS::Root();
        t->SetBranchAddress("ds", &ds);

	Int_t nEvents = t->GetEntries();

	Int_t num_events = nEvents;
	
	TH1D *pmtbcharge[n_hist];
	TH1D *pmtbtime[n_hist];	

	for(int a0 =0; a0<n_hist;a0++){
		//define the mean histogram for loop 0, else define PMT histograms
				TString pst1s("PMTq"+ convertInt(a0));
				TString pst2s("Block Charge for PMT " + convertInt(a0));
				pmtbcharge[a0] = new TH1D(pst1s,pst2s,10000,0,10000);
				
		
				TString tst1s("PMTt"+ convertInt(a0));
				TString tst2s("Block time for PMT " + convertInt(a0));
				pmtbtime[a0] = new TH1D(tst1s,tst2s,h_bins,0,window_size);
					
		}//for
//Define output TTree
	//Output file
	TFile *qt_out = new TFile(opath + "qt.root","recreate");	

	//Run name
	TString run_name("Dark_Run");

	TTree * qt = new TTree(run_name,"drid");	
	
	Double_t block_charge, block_time;
	Int_t pmtidn;

	qt->Branch("pmtidn",&pmtidn,"pmtidn/I");
	qt->Branch("block_charge",&block_charge,"block_charge/D");
	qt->Branch("block_time",&block_time,"block_charge/D");


//Loop through events
        for (int i=0;i<num_events;i++){

        if(i%10000==0){ 
        cout << progress(i,num_events) <<  " and we are at event "<< i << endl;;
        }
                t->GetEntry(i);

                Int_t cal_count = ds->GetCALCount();

                for(int am1 = 0;am1<cal_count;am1++){
                        RAT::DS::CAL *cal = ds->GetCAL(am1);

                        Int_t pmt_count = cal->GetPMTCount();



                        for(int a1 =0;a1<pmt_count;a1++){

                        RAT::DS::PMT *pmt = cal->GetPMT(a1);//CHANGED NUMBER

                                int pmtid =  pmt->GetID(); //change this if the pmts are plugged out of order
				pmtidn = pmtid;

                                Int_t block_count = pmt->GetBlockCount();

                                for(int ab2 =0;ab2 < block_count ; ab2++){

                                        RAT::DS::Block *block = pmt->GetBlock(ab2);
                                        block_time =  block->GetTime();
                                        block_charge = block->GetIntegral();
				
					pmtbcharge[pmtid]->Fill(block_charge);
					pmtbtime[pmtid]->Fill(block_time);	
					
					qt->Fill();
				}//block
			}//pmt
		}//cal
	}//num events

int har[n_hist];

for(int p =0; p< n_hist; p++){
	pmtbcharge[p]->GetXaxis() ->SetRange(5,100);
	Double_t maxbin = pmtbcharge[p]->GetMaximumBin();
	cout << "peak bin value " << maxbin << " for PMT "<< p<< endl;
	har[p] = maxbin;
}


TFile *tfile[n_hist], *qfile[n_hist];
for(int a8 =0; a8<n_hist ;a8++){
		TString tfnm("tPMT"+convertInt(a8) + ".root");
		tfile[a8] = new TFile(opath + tfnm, "recreate");	
		pmtbtime[a8]->Write();
		tfile[a8]->Close();


		TString qfnm("qPMT"+convertInt(a8) + ".root");
		qfile[a8] = new TFile(opath + qfnm, "recreate");	
		pmtbcharge[a8]->GetXaxis()->SetRange(0,80);
		pmtbcharge[a8]->Write();
		qfile[a8]->Close();
}

qt_out->cd();
qt->Write();
qt_out->Close();


/*
//Plots		
TCanvas *ip= new TCanvas("a0","a0");
ip->Divide(2,2);

for(int u =1; u < n_hist; u++){
TPad *ip1 = (TPad*)(ip->cd(u));
pmtbcharge[u]->Draw();
pmtbcharge[u]->GetXaxis()->SetRange(0,120); 
}

TCanvas *jp= new TCanvas("a1","a1");
pmtbcharge[0]->Draw();
pmtbcharge[0]->GetXaxis()->SetRange(0,120); 
*/


}//spe calib		


//
//	
//Convert Int
//
//

string convertInt(int number){
	stringstream ss;
	ss<< number;
	string sss = ss.str();
	return sss;

}//string conversion

//
//
//sortFunc
//
//

bool sortFunc( const vector<Double_t>& p1, const vector<Double_t>& p2 ) {
 return p1[0] < p2[0];
}


//
//
// progess track
//
//

string progress(int iter, int n_events){
	int c_p,p_p;;
	string prog;
	c_p = 100*iter/n_events;

	prog = convertInt(c_p);
	prog = "We are " +  convertInt(c_p) + " % of the way there";
	return prog;
//	}
}
