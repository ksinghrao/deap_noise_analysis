/*            _   __   _   _   _    _                
	|  | |   |  | | \ |   | \  /                  
	|  | |   |  | | | |   |_/  \_
        |__| |_  |__| | | |_  |\     \
        |  | |   |  | | | |   | \    |
        |  | |__ |  | |_/ |__ |  \ __/
*/
//c++ headers


#include<TChain.h>
#include<TH1F.h>
#include<RAT/DS/Root.hh>
#include<RAT/DS/CAL.hh>
#include<RAT/DS/PMT.hh>
#include<RAT/DS/Pulse.hh>
#include<RAT/DS/Block.hh>
#include<TCanvas.h>
#include<TApplication.h>
#include<TH2F.h>
#include<TFile.h>
#include<TMath.h>>
#include<RAT/DS/TS.hh>
#include<TLine.h>
#include<TBox.h>
#include<TAttLine.h>
#include<TAttFill.h>
#include<TColor.h>
#include<TText.h>
#include<TAttText.h>
#include<TLatex.h>
#include<TROOT.h>
#include<RAT/DS/CAL.hh>
#include<RAT/DS/QT.hh>
#include<TDirectory.h>
#include<TNtuple.h>
#include<TTree.h>
//#include<RAT/CAENDigitizer.hh>
#include<TF1.h>


//C++
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<string>
#include<vector>
#include<sstream>
#include<math.h>

using namespace std;

#include "pulsefit.h";


/*	       __
        |\ /| |  | | |\  |
        | | | |  | | | | |
        |   | |__| | | | |
        |   | |  | | | | |
        |   | |  | | |  \|
*/

int main(int argc, char ** argv){
TApplication theApp("App",&argc,argv);

read_file();
fit_analysis();


cout << "! DONE !" << endl;

theApp.Run();

return 0;

}
 
/*    	 __       __   _   __       _
 	|  | |   |  | | \ |  | |   /
	|    |   |  | |_/ |  | |   \_
	| _  |   |  | | \ |__| |     \
	|  | |   |  | | | |  | |     |
	|_/  |__ |__| |_/ |  | |__ __/
*/
int initpmtid = 0;
int numpmt = 255;


int n_hist = 255;
int npulse =2;
//name of data type



TString inpath("/home/singhrao/Documents/DEAP_noise_analysis/March15th_Dark_run/data/delta_t/PMT_delta_t/PMT_DT_plots/");
TString opath("/home/singhrao/Documents/DEAP_noise_analysis/March15th_Dark_run/data/fit_db/PMT_fits/");


/*       __   __  ___  _
        |    |     |  /
        |__  |     |  \_
        |    |     |    \
        |    |     |    |
        |    |__   |  __/
*/





//
//
//Read and fit files
//
//

void read_file(){
//
// Get PMT
//
TFile *pmtdelt[n_hist];

Double_t fittedDNrate, fittedAPprob,fittedDNrate_error,fittedAPprob_error, napulses, fittedAP1prob, fittedAP1prob_error, fittedAP2prob, fittedAP2prob_error;
Int_t pmtID;

for(int pmtnum =initpmtid; pmtnum< numpmt; pmtnum++){
	//Define TH2D for prelimiary studies

	//Define output tree
	TString fid(opath + "PMTfit" + convertInt(pmtnum) + ".root");
	TFile *fitinfo = new TFile(fid,"recreate");
		
	TString pmtidr("PMTfp" + convertInt(pmtnum));
	TTree *dtfitinfo = new TTree(pmtidr,pmtidr);


	dtfitinfo->Branch("pmtID",&pmtID,"pmtID/I");
	dtfitinfo->Branch("napulses",&napulses,"napulses/D");
	dtfitinfo->Branch("fittedDNrate",&fittedDNrate,"fittedDNrate/D");	
	dtfitinfo->Branch("fittedDNrate_error",&fittedDNrate_error,"fittedDNrate_error/D");	
	dtfitinfo->Branch("fittedAPprob",&fittedAPprob,"fittedAPprob/D");
	dtfitinfo->Branch("fittedAPprob_error",&fittedAPprob_error,"fittedAPprob_error/D");
	dtfitinfo->Branch("fittedAP1prob",&fittedAP1prob,"fittedAP1prob/D");
	dtfitinfo->Branch("fittedAP1prob_error",&fittedAP1prob_error,"fittedAP1prob_error/D");
	dtfitinfo->Branch("fittedAP2prob",&fittedAP2prob,"fittedAP2prob/D");
	dtfitinfo->Branch("fittedAP2prob_error",&fittedAP2prob_error,"fittedAP2prob_error/D");


	//file name
	TString pmtnm("pmt" + convertInt(pmtnum) + ".root");
	TFile *f1 = new TFile(inpath + pmtnm);
	//pmtid
	//TString pmtid("qdt123_px");	
	TString pmtid("qdt" + convertInt(pmtnum) +"_px");	
	TH1D * delta_pmt_t = (TH1D*)f1->Get(pmtid);

	//define fit function with variable number of afterpulses

	TF1 *npulsespec[npulse];
	//AP2 (from DN spectrum on right to left)
	//defined npulses
	npulsespec[0] = new TF1("ap2_0",ap_fct,0,1E9,3);
	npulsespec[0]->SetParameter(1,3.0E-01 );
	npulsespec[0]->SetParameter(2,6.6E+03 );
	npulsespec[0]->SetParameter(3,600 );

	npulsespec[1] = new TF1("ap2_1",ap_fct,0,1E9,3);
	npulsespec[1]->SetParameter(1,2.0E-01 );
	npulsespec[1]->SetParameter(2,1.5E+03 );
	npulsespec[1]->SetParameter(3,400 );
//
//build complete spectrum
//	
	//fit range
	int l_limit = 2E2;
	int h_limit = 1E9;

	TF1 *seven_p_spec = new TF1("7p_spec",sevenpulsefit,l_limit,h_limit,3*npulse+1);

	//Set AP and DN parameters and names
	int kl =0;
	seven_p_spec->SetParameter(0,4e-06);
	seven_p_spec->SetParName(0,"Dark noise rate");


	seven_p_spec->SetParameter(1,5e-01 );
	seven_p_spec->SetParameter(2,6.26773e+03 );
	seven_p_spec->SetParameter(3,1.43341e+03 );

	seven_p_spec->SetParameter(4,12e-02 );
	seven_p_spec->SetParameter(5,1.57247e+03 );
	seven_p_spec->SetParLimits(5,2E2,4E3);//Limited afterpulse peak time to physical values
	seven_p_spec->SetParameter(6,8.19436e+02 );


//BROKEN NEEDS FIXING
/*	
	for(int ij =0; ij<npulse ;ij++){
		for(int jj =0;jj<3;jj++){
				cout << kl ++ << " "<<  ij << " " <<jj%3 << endl;
				if(kl !=0) {
				seven_p_spec->SetParameter(kl,npulsespec[ij]->GetParameter(jj%3));
//				cout << "parameter number "<< kl << endl;
			}
	
		}
	}
*/


	for(int kj =0; kj <npulse; kj++){
			
		TString nm1("AP" + convertInt(kj+1) + " Prob" );
		TString nm2("AP" + convertInt(kj+1) + " Peak");
		TString nm3("AP" + convertInt(kj+1) + " Variance");

	
		seven_p_spec->SetParName(3*kj+1,nm1);
		seven_p_spec->SetParName(3*kj+2,nm2);
		seven_p_spec->SetParName(3*kj+3,nm3);
	
		}//for kj
	
//Check if population is correct

	//check if population is correct
	for(int k=0;k<3*npulse +1 ;k++){

		cout << "parameter id: " << k  << " parameter name "<< seven_p_spec->GetParName(k) << " parameter value: "<< seven_p_spec->GetParameter(k) << endl;
	}//for k	


//
//fit pmt
//
	//Plot test
//	TCanvas *can1 = new TCanvas("can1","can1");
//	can1->Divide(1,1);

//	TPad *pad1 = (TPad*)(can1->cd(1));
//	pad1->SetLogx();
	//Draw
//	delta_pmt_t->Rebin(10);
//	delta_pmt_t->Draw();
//	seven_p_spec->Draw("same");

	
	//Fit
	delta_pmt_t->Fit(seven_p_spec,"R");

//
//calculate ap probablilties and dn rates
//

//fitted ap prob and dn rate
if(seven_p_spec->GetParameter(0) > 0){

fittedAP1prob = seven_p_spec->GetParameter(1);
fittedAP1prob_error = seven_p_spec->GetParError(1) ;

fittedAP2prob = seven_p_spec->GetParameter(4);
fittedAP2prob_error = seven_p_spec->GetParError(4) ;

fittedAPprob = seven_p_spec->GetParameter(1)+seven_p_spec->GetParameter(4);
fittedAPprob_error = TMath::Sqrt(pow( seven_p_spec->GetParError(1) ,2 )+ pow( seven_p_spec->GetParError(4), 2) ) ;
fittedDNrate = 1E9*seven_p_spec->GetParameter(0); //1E9 <=> ns to s
fittedDNrate_error = 1E9*seven_p_spec->GetParError(0); //1E9 <=> ns to s


napulses = npulse;
pmtID = pmtnum;

dtfitinfo->Fill();
}

else{

fittedAP1prob = -1;
fittedAP1prob_error = -1;
fittedAP2prob = -1;
fittedAP2prob_error = -1;
fittedAPprob = -1;
fittedAPprob_error = -1;
fittedDNrate = -1;
fittedDNrate_error = -1;


napulses = npulse;
pmtID = pmtnum;

dtfitinfo->Fill();

}
//
//write ofile
//

fitinfo->cd();
dtfitinfo->Write();
fitinfo->Close();

TString speco("PMT_fdist" + convertInt(pmtnum) + ".root");
TFile * PMTout = new TFile(opath+speco,"recreate");
delta_pmt_t->Write();
PMTout->Close();		

}//for pmt loop


}

void fit_analysis(){

//Out files
TH1D *darkrates = new TH1D("dark_rates","dark_rates",500,0,20E3);
TH1D *approb = new TH1D("approb","approb",100,0,1);
TH1D *approb1 = new TH1D("approb1","approb1",100,0,1);
TH1D *approb2 = new TH1D("approb2","approb2",100,0,1);

TH1D *pmtrates = new TH1D("IDrates","ID_rates",numpmt,0,numpmt);
TH1D *pmtprob = new TH1D("IDprob","IDprob",numpmt,0,numpmt);
TH1D *pmtprobap1 = new TH1D("IDprobap1","IDprobap1",numpmt,0,numpmt);
TH1D *pmtprobap2 = new TH1D("IDprobap2","IDprobap2",numpmt,0,numpmt);

//Input tree
TString infile("/home/singhrao/Documents/DEAP_noise_analysis/March15th_Dark_run/data/fit_db/PMT_fits/");



for(int iter1 =initpmtid; iter1<numpmt; iter1++ ){
        TString fid(infile + "PMTfit" + convertInt(iter1) + ".root");
//  	cout << fid << endl;
        TFile *fitinfo = new TFile(fid);

        TString pmtidr("PMTfp" + convertInt(iter1));
//	cout << pmtidr << endl;
	TTree *apdnana = (TTree*)fitinfo->Get(pmtidr);


	Double_t fittedDNrate,fittedAPprob,napulses,fittedDNrate_error, fittedAPprob_error, fittedAP1prob, fittedAP1prob_error, fittedAP2prob, fittedAP2prob_error;

	Int_t pmtID;

	apdnana->SetBranchAddress("pmtID",&pmtID);
	apdnana->SetBranchAddress("napulses",&napulses);
	apdnana->SetBranchAddress("fittedDNrate",&fittedDNrate);
	apdnana->SetBranchAddress("fittedDNrate_error",&fittedDNrate_error);
	apdnana->SetBranchAddress("fittedAPprob",&fittedAPprob);
	apdnana->SetBranchAddress("fittedAPprob_error",&fittedAPprob_error);
	apdnana->SetBranchAddress("fittedAP1prob",&fittedAP1prob);
	apdnana->SetBranchAddress("fittedAP1prob_error",&fittedAP1prob_error);
	apdnana->SetBranchAddress("fittedAP2prob",&fittedAP2prob);
	apdnana->SetBranchAddress("fittedAP2prob_error",&fittedAP2prob_error);

	apdnana->GetEntry(0);

	cout << "fittedDNrate " << fittedDNrate << " fittedAPprob " << fittedAPprob << " pmtID " << pmtID << endl;
	darkrates->Fill(fittedDNrate);
	approb->Fill(fittedAPprob);
	approb1->Fill(fittedAP1prob);
	approb2->Fill(fittedAP2prob);

pmtrates->SetBinContent(iter1,fittedDNrate);
pmtrates->SetBinError(iter1,fittedDNrate_error);

pmtprob->SetBinContent(iter1,fittedAPprob);	
pmtprob->SetBinError(iter1,fittedAPprob_error);

pmtprobap1->SetBinContent(iter1,fittedAP1prob);	
pmtprobap1->SetBinError(iter1,fittedAP1prob_error);	


pmtprobap2->SetBinContent(iter1,fittedAP2prob);	
pmtprobap2->SetBinError(iter1,fittedAP2prob_error);	


}


TCanvas *anacan1 = new TCanvas("anacan1","anacan1",800,600);
darkrates->GetXaxis()->SetTitle("Rate (Hz)");
darkrates->SetTitle("Dark Rates (Dark run)");
darkrates->Draw();

TCanvas *anacan2 = new TCanvas("anacan2","anacan2",800,600);
approb->GetXaxis()->SetTitle("Nomalized Probability");
approb->SetTitle("Afterpulse probablility (Dark run)");
approb->Draw();

TCanvas *anacan3 = new TCanvas("anacan3","anacan3",800,600);
pmtrates->GetYaxis()->SetTitle("Rate (Hz)");
pmtrates->GetXaxis()->SetTitle("PMT ID");
pmtrates->SetTitle("Rate for each PMT (Dark Run)");
pmtrates->Draw();

TCanvas *anacan4 = new TCanvas("anacan4","anacan4",800,600);
pmtprob->GetYaxis()->SetTitle("Probability ");
pmtprob->GetXaxis()->SetTitle("PMT ID");
pmtprob->SetTitle("Afterpulsing probability for each PMT (Dark Run) ");
pmtprob->Draw();

TCanvas *anacan5 = new TCanvas("anacan5","anacan5",800,600);
pmtprobap1->GetYaxis()->SetTitle("Probability ");
pmtprobap1->GetXaxis()->SetTitle("PMT ID");
pmtprobap1->SetTitle("Afterpulsing probability for each PMT (Dark Run) ");
pmtprobap1->Draw();


TCanvas *anacan6 = new TCanvas("anacan6","anacan6",800,600);
pmtprobap2->GetYaxis()->SetTitle("Probability ");
pmtprobap2->GetXaxis()->SetTitle("PMT ID");
pmtprobap2->SetTitle("Afterpulsing probability for each PMT (Dark Run) ");
pmtprobap2->Draw();

//
//Write files
TFile *prob = new TFile(opath +"approb.root","recreate");
approb->Write();
approb1->Write();
approb2->Write();
prob->Close();

TFile *rates = new TFile(opath + "drates.root","recreate");
darkrates->Write();
rates->Close();

TFile *pmprob = new TFile(opath + "pmtprob.root","recreate");
pmtprobap1->Write();
pmtprobap2->Write();
pmtprob->Write();
pmprob->Close();

//TFile *pmprobap1 = new TFile(opath + "pmtprobap1.root","recreate");
//pmprobap1->Close();

//TFile *pmprobap2 = new TFile(opath + "pmtprobap2.root","recreate");
//pmprobap2->Close();

TFile *pmrates = new TFile(opath + "pmtrates.root","recreate");
pmtrates->Write();
pmrates->Close();

//


}//fit analysis

double ap_fct(double *x, double *p){

        double AP;

        AP = p[0]*TMath::Exp(-1* pow( (x[0] - p[1]),2 ) / (2*pow( (p[2]) ,2)))*(1 /(TMath::Sqrt(2*TMath::Pi())*p[2] )) ;

        return AP;

}




double sevenpulsefit(double *x, double *p){
	
	double nDN,DN;	
	double ap[npulse-1];
	double nap[npulse-1];

DN = p[0]*TMath::Exp(-p[0]*x[0]);
nDN = 1+TMath::Exp(-x[0]*p[0]);


for(int ij =0; ij<npulse;ij++){

	ap[ij] = p[3*ij+1]*TMath::Exp(-1* pow( (x[0] - p[3*ij+2]),2 ) / (2*pow( (p[3*ij+3]) ,2)))/(TMath::Sqrt(2*TMath::Pi())*p[3*ij+3] );

	nap[ij] = 1-0.5*p[3*ij+1]*(1+TMath::Erf((x[0] - p[3*ij+2]) /(TMath::Sqrt(2)*p[3*ij+3])));
	
//`	cout << "AP# " << ij  << " AP prob " << 3*ij +1 << " AP time " <<  3*ij +2 <<  " AP width "<< 3*ij+3<< endl;

}
//Dark noise tail debug
//	return DN;

//Dark noise + 1 afterpulse
//	return DN*nap[0] + nDN*ap[0];


//Dark noise + 2 afterpulses

	return DN*nap[0]*nap[1] +
		nDN*ap[0]*nap[1] +
		nDN*nap[0]*ap[1] ;


}//7pulsef



//
//
//Convert Int
//
//

string convertInt(int number){
        stringstream ss;
        ss<< number;
        string sss = ss.str();
        return sss;

}//string conversion

