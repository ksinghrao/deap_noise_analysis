#ifndef dt.h
#define dt.h


void sequencer();

void qt_block();

string progress(int iter, int n_events);

string convertInt(int number);

bool sortFunc( const vector<Double_t>& p1, const vector<Double_t>& p2 );




#endif
